using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
public class GunController : MonoBehaviour
{
    private GunAnimationController _gunAnim;

    private Camera _FPSCam;
    [SerializeField]private GameObject _gunPoint;
    [SerializeField] private float _range = 1000;
    [SerializeField] private int _damage = 100;
    [SerializeField] private int _fireRate = 15;

     //private int _ammoInInv = 40;
    private int _ammoInGun = 0;
    private int _maxAmmoInGun = 12;
    

    [SerializeField] private GameObject _muzzleEffect;
    [SerializeField] private GameObject _impactEffect;
    [SerializeField] private AudioSource _GunShoot;
    [SerializeField] private AudioSource _emptyMag;


    private float _nextTimeToFire = 3;
    private void Awake()
    {
        _GunShoot = GetComponent<AudioSource>();
        _gunAnim = GetComponent<GunAnimationController>();
        _FPSCam = transform.parent.parent.parent.parent.GetComponent<Camera>();
    }
    private void Start()
    {
        UIManager.Instance.UpdateAmmo(_ammoInGun);
        AmmoManager.Instance.UpdateUI();
    }
    void Update()
    {
            
        if (Input.GetKeyDown(KeyCode.Mouse0)&& Time.time >= _nextTimeToFire)
        {
            if (_ammoInGun > 0)
            {
                _ammoInGun--;
                _nextTimeToFire = Time.time + 1f / _fireRate;
                GunShoot();
            }
            else
            {
                _emptyMag.Play();
            }
           
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }

    }
    
    public void GunShoot()
    {
        _GunShoot.Play();
        UIManager.Instance.UpdateAmmo(_ammoInGun);
        _gunAnim.ShootAnim();
        LeanPool.Spawn(_muzzleEffect, _gunPoint.transform.position, Quaternion.identity) ;
        RaycastHit hit;
        if (Physics.Raycast(_FPSCam.transform.position, _FPSCam.transform.forward, out hit, _range))
        {
           
            ITakeDamage mb = hit.transform.GetComponent<ITakeDamage>();

            if (mb != null)
            {
                mb.GetDMG(_damage);
            }
            LeanPool.Spawn(_impactEffect, hit.point , Quaternion.LookRotation(hit.normal));
        }
    }
    private void Reload()
    {
        if (AmmoManager.Instance.currentAmmo>0)
        {
            _gunAnim.Reload();
            AmmoManager.Instance.currentAmmo -= _maxAmmoInGun;
            if (AmmoManager.Instance.currentAmmo < 0)
            {
                _ammoInGun = (_maxAmmoInGun + AmmoManager.Instance.currentAmmo);
                AmmoManager.Instance.currentAmmo = 0;
            }
            else
                _ammoInGun = _maxAmmoInGun;
        }
        //else
        //{
        //    _gunAnim.Reload();
        //    AmmoManager.Instance.currentAmmo -= _maxAmmoInGun;
        //    if (AmmoManager.Instance.currentAmmo < 0)
        //    {
        //        _ammoInGun = (_maxAmmoInGun + AmmoManager.Instance.currentAmmo);
        //        AmmoManager.Instance.currentAmmo = 0;
        //    }
        //    else
        //        _ammoInGun = _maxAmmoInGun;
        //}
        AmmoManager.Instance.UpdateUI();
        UIManager.Instance.UpdateAmmo(_ammoInGun);
    }
}


