using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunAnimationController : MonoBehaviour
{
    public bool _isReloading;
    public Animator _anim;
    private string _shootAnimName ="Shoot";
    private string _reloadAnimName ="Reload";
    public void ShootAnim()
    {
        _anim.Play(_shootAnimName);
    }
    public void Reload()
    {
        _anim.Play(_reloadAnimName);
    }


}
