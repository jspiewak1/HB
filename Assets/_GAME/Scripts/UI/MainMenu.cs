using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public void StartGame()
    {
        LevelManageer.Instance.StartGame();
    }
    public void Quit()
    {
        LevelManageer.Instance.Quit();
    }
}
