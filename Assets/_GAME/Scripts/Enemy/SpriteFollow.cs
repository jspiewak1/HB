using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFollow : MonoBehaviour
{
    private void Update()
    {
        Vector3 player = PlayerPositionVectorManager.Instance.Player;
        player = new Vector3(player.x, transform.position.y, player.z);
        this.transform.LookAt(player);
    }
}
