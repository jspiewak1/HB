using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Rigidbody _rb = null;
    [SerializeField]private float _speed = 0;
    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();     
    }
    void Update()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float z = Input.GetAxisRaw("Vertical");
        Vector3 moveBy = transform.right * x + transform.forward * z;
        _rb.MovePosition(transform.position + moveBy.normalized *_speed* Time.deltaTime);
    }
}
