using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour
{
    [SerializeField] private GameObject _pausePanel;
    private void Start()
    {
        _pausePanel.SetActive(false);
        Time.timeScale = 1;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }
    private void Pause() 
    {
        _pausePanel.SetActive(!_pausePanel.activeSelf);
        Time.timeScale = 1 - Time.timeScale;
    }
    public void Quit()
    {
        Application.Quit();
    }

}
