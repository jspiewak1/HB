using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoManager : MonoSingleton<AmmoManager>
{
    public int currentAmmo;
    public int startingAmmo = 40;
    private void Start()
    {
        currentAmmo = startingAmmo;
        UpdateUI();
    }

    public void UpdateUI()
    {
        UIManager.Instance.UpdateAllAmmo(currentAmmo);
    }
    public void AddAmmunition(int ammo)
    {
        currentAmmo += ammo;
        UpdateUI();
    }

}
