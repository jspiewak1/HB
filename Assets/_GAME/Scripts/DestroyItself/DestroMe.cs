using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
public class DestroMe : MonoBehaviour
{
    [SerializeField] private float _delay = 5;
    private void Start()
    {
        LeanPool.Despawn(this.gameObject, _delay);
    }
}
