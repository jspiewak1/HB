using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoRefil : MonoBehaviour
{
    private int _ammoAmount = 10;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerHp>())
        {
            AmmoManager.Instance.AddAmmunition(_ammoAmount);
            Destroy(this.gameObject);
        }
    }
}
