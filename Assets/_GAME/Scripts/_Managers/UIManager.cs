using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField] private TextMeshProUGUI _ammoInGun;
    [SerializeField] private TextMeshProUGUI _ammoAll;
    public void UpdateAmmo(int amount)
    {
        _ammoInGun.text = amount.ToString();
    }
    public void UpdateAllAmmo(int amount)
    {
        _ammoAll.text = amount.ToString();
    }
    

}
