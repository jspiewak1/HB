using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(FieldOfView))]
public class AIBehaviour : MonoBehaviour
{
    public NavMeshAgent agent;
    [SerializeField] private Transform _player;
    [SerializeField] private LayerMask _playerLayer, _groundLayer;


    //player seeing
    private FieldOfView _seenObjects;
    private float _howLongRememberPlayer = 20;

    //Patrolling
    private Vector3 _walkPoint;
    private bool _walkPointSet;
    private float _walkPointRange = 10;

    //Attacking
    [SerializeField] private float _timeBetweenAttacks = 1;
    private bool _alreadyAttacked;
    

    //states
    private bool _meRememberPlayerPosition = false;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        _seenObjects = GetComponent<FieldOfView>();
    }
    private void Update()
    {
        if (_seenObjects.visibleTargets.Count > 0) //widz� playera, to go gonie i strzelam
        {
            ChasePlayer();
            Attack();
        }
        else if (_meRememberPlayerPosition) //nie widz� go ale pami�tam gdzie jest, ide tam
        {
            LookingForPlayer();
        }
        else  //znikn�� :(, ide gdzies 
        {
            Patrolling();
        }
    }

    private void Patrolling()
    {
        if (!_walkPointSet)
        {
            FindWalkPoint();
        }
        else
        {
            agent.SetDestination(_walkPoint);
        }
        Vector3 distanceToWalkPoint = transform.position - _walkPoint;
        if (distanceToWalkPoint.magnitude <2f)
        {
            _walkPointSet = false;
        }
    }
    private void FindWalkPoint() 
    {
        float randomX = Random.Range(-_walkPointRange, _walkPointRange);
        float randomZ = Random.Range(-_walkPointRange, _walkPointRange);

        _walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        if (Physics.Raycast(_walkPoint,-transform.up, _groundLayer))
        {
            _walkPointSet = true;
        }
    }
    
    private void ChasePlayer()
    {
        _player = _seenObjects.visibleTargets[0].gameObject.transform;
        _meRememberPlayerPosition = true;
        agent.SetDestination(_player.position);
    }
    private void LookingForPlayer()
    {
        agent.SetDestination(_player.position);
        
        if (!FBPisRunning)
        {
            StartCoroutine(ForgettingBoutPlayer());
        }
    }
    private bool FBPisRunning = false;
    IEnumerator ForgettingBoutPlayer()
    {
        FBPisRunning = true;
        Debug.Log("Zacz��em zapomina�");
        yield return new WaitForSeconds(_howLongRememberPlayer);
        FBPisRunning = false;
        _meRememberPlayerPosition = false; //zapomnia�em gdzie jest gracz :/
        Debug.Log("Zapomnia�em");
    }

    [SerializeField] private EnemyWeapon _weapon;
    private void Attack()
    {
        if (!_alreadyAttacked)
        {
            _weapon.Attack();
            _alreadyAttacked = true;
            Invoke(nameof(Reset), _timeBetweenAttacks);
        }
        
    }

    private void Reset()
    {
        _alreadyAttacked = false;
    }

}
