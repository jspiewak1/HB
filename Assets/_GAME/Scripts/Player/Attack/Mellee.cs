using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mellee : MonoBehaviour
{
    [SerializeField] private Transform _attackPoint;
    [SerializeField] private float _range = 0.5f;
    [SerializeField] private int _damage = 10;
    [SerializeField] private LayerMask _enemyLayer;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            Attack();
        }    
    }
    private void Attack()
    {

        Collider[] hitEnemies = Physics.OverlapSphere(_attackPoint.position, _range, _enemyLayer);
        
        foreach (Collider enemy in hitEnemies)
        {
            ITakeDamage testInterface = enemy.transform.GetComponent<ITakeDamage>();

            if (testInterface != null)
            {
                testInterface.GetDMG(_damage);
            }
            Debug.Log("We Hit: " + enemy.name);
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(_attackPoint.position, _range);
    }
}
