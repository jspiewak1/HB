using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
public class EnemyWeapon : MonoBehaviour
{
    [SerializeField] private GameObject _gunPoint;
    [SerializeField] private GameObject _bullet;
    public void Attack()
    {
        LeanPool.Spawn(_bullet, _gunPoint.transform); 
     }
}
