using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody _rb;
    private float speed = 200f;
    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        _rb.AddForce(transform.forward * speed);
    }
    private void Start()
    {
        Destroy(this.gameObject, 5);   
    }
    private int _dmg = 5;
    private void OnTriggerEnter(Collider other)
    {
        ITakeDamage mb = other.transform.GetComponent<ITakeDamage>();

        if (mb != null)
        {
            mb.GetDMG(_dmg);
        }
    }
}
